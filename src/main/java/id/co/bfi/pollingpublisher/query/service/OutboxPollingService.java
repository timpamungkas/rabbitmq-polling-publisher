package id.co.bfi.pollingpublisher.query.service;

import java.time.LocalDateTime;
import java.util.List;

import id.co.bfi.pollingpublisher.entity.outboxpolling.OutboxPolling;

public interface OutboxPollingService<T extends OutboxPolling> {

	void convertAndPublish(T o) throws Exception;

	List<T> findUnpublishedOutboxPolling();

	void deletePublishedBefore(LocalDateTime deleteDate);

}
