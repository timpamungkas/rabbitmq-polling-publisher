package id.co.bfi.pollingpublisher.query.action;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate.ConfirmCallback;
import org.springframework.amqp.rabbit.core.RabbitTemplate.ReturnCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import id.co.bfi.pollingpublisher.broker.message.OutboxPollingMessage;
import id.co.bfi.pollingpublisher.entity.outboxpolling.OutboxPolling;
import id.co.bfi.pollingpublisher.repository.outboxpolling.OutboxPollingRepository;

public abstract class OutboxPollingAction<T extends OutboxPolling> {

	private static final Logger LOG = LoggerFactory.getLogger(OutboxPollingAction.class);

	@Autowired
	private OutboxPollingRepository<T> outboxRepository;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	private final Class<T> typeParameterClass;

	public OutboxPollingAction(Class<T> typeParameterClass) {
		this.typeParameterClass = typeParameterClass;
	}

	private ConfirmCallback buildConfirmCallback() {
		return new ConfirmCallback() {
			@Override
			public void confirm(CorrelationData correlationData, boolean ack, String cause) {
				if (correlationData == null) {
					return;
				}

				var id = Long.valueOf(correlationData.getId()).longValue();
				if (ack) {
					var message = correlationData.getReturnedMessage();
//					outboxRepository.deleteById(id);
					outboxRepository.updatePublished(id);
				} else {
					var data = outboxRepository.findById(id);
					data.ifPresent(d -> {
						d.setNote("Invalid exchange");
						d.setPublished("FAILED");
						outboxRepository.save(d);
					});
				}
			}
		};
	}

	private ReturnCallback buildReturnCallback() {
		return new ReturnCallback() {
			@Override
			public void returnedMessage(Message message, int replyCode, String replyText, String exchange,
					String routingKey) {
				if (replyText.equalsIgnoreCase("NO_ROUTE")) {
					try {
						// reinsert with message
						T reinsertOutbox = createInvalidRoutingKeyOutboxPolling(typeParameterClass, message, exchange,
								routingKey);
						outboxRepository.save(reinsertOutbox);
					} catch (Exception e) {
						LOG.error("Error for exchange : {}, routingKey : {}, body : {}, cause {}", exchange, routingKey,
								new String(message.getBody()), e.getMessage());
					}
				}
			}
		};
	}

	public void convertAndPublish(T outboxPolling) throws IOException, SQLException {
		var outboxPollingMessage = new OutboxPollingMessage();
		var correlationData = new CorrelationData(Long.toString(outboxPolling.getId()));

		outboxPollingMessage.setId(outboxPolling.getId());
		outboxPollingMessage.setExchange(outboxPolling.getExchange());
		outboxPollingMessage.setRoutingKey(outboxPolling.getRoutingKey());
		outboxPollingMessage.setChangedData(outboxPolling.getChangedData());

		rabbitTemplate.convertAndSend(outboxPollingMessage.getExchange(), outboxPollingMessage.getRoutingKey(),
				outboxPollingMessage.getChangedData(), correlationData);
		// delete when publisher ack via confirm callback
	}

	private T createInvalidRoutingKeyOutboxPolling(Class<T> clazz, Message message, String exchange, String routingKey)
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			NoSuchMethodException, SecurityException {
		T reinsertOutbox = clazz.getDeclaredConstructor().newInstance();

		var id = Long.parseLong(message.getMessageProperties().getHeader("spring_returned_message_correlation"));

		reinsertOutbox.setId(id);
		reinsertOutbox.setChangedData(new String(message.getBody()));
		reinsertOutbox.setCreatedDate(LocalDateTime.now());
		reinsertOutbox.setExchange(exchange);
		reinsertOutbox.setRoutingKey(routingKey);
		reinsertOutbox.setPublished("FAILED");
		reinsertOutbox.setNote("Invalid routing key");

		return reinsertOutbox;
	}

	public void deletePublishedBefore(LocalDateTime deleteDate) {
		outboxRepository.deletePublishedBefore(deleteDate);
	}

	public List<T> findAllOutboxPolling() {
		final var allOutboxData = new ArrayList<T>();
		outboxRepository.findAll(Sort.by("id").ascending()).forEach(allOutboxData::add);

		return allOutboxData;
	}

	public List<T> findUnpublishedOutboxPolling() {
		final var allOutboxData = new ArrayList<T>();
		outboxRepository.findByPublishedIsNullOrderByIdAsc().forEach(allOutboxData::add);

		return allOutboxData;
	}

	@PostConstruct
	private void postConstruct() {
		this.rabbitTemplate.setConfirmCallback(buildConfirmCallback());
		this.rabbitTemplate.setReturnCallback(buildReturnCallback());
	}

}