package id.co.bfi.pollingpublisher.query.action;

import org.springframework.stereotype.Component;

import id.co.bfi.pollingpublisher.entity.outboxpolling.local.OutboxPolling_Local;

@Component
public class OutboxPollingAction_Local extends OutboxPollingAction<OutboxPolling_Local> {

	public OutboxPollingAction_Local() {
		super(OutboxPolling_Local.class);
	}

}
