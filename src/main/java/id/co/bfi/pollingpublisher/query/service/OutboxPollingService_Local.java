package id.co.bfi.pollingpublisher.query.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.bfi.pollingpublisher.entity.outboxpolling.local.OutboxPolling_Local;
import id.co.bfi.pollingpublisher.query.action.OutboxPollingAction;

@Service
public class OutboxPollingService_Local implements OutboxPollingService<OutboxPolling_Local> {

	@Autowired
	private OutboxPollingAction<OutboxPolling_Local> action;

	@Override
	public void convertAndPublish(OutboxPolling_Local outboxPolling) throws Exception {
		action.convertAndPublish(outboxPolling);
	}

	@Override
	public void deletePublishedBefore(LocalDateTime deleteDate) {
		action.deletePublishedBefore(deleteDate);
	}

	@Override
	public List<OutboxPolling_Local> findUnpublishedOutboxPolling() {
		return action.findUnpublishedOutboxPolling();
	}

}
