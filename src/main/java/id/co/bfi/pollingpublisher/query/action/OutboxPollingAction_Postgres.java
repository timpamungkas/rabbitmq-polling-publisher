package id.co.bfi.pollingpublisher.query.action;

import org.springframework.stereotype.Component;

import id.co.bfi.pollingpublisher.entity.outboxpolling.postgres.OutboxPolling_Postgres;

@Component
public class OutboxPollingAction_Postgres extends OutboxPollingAction<OutboxPolling_Postgres> {

	public OutboxPollingAction_Postgres() {
		super(OutboxPolling_Postgres.class);
	}

}
