package id.co.bfi.pollingpublisher.query.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.bfi.pollingpublisher.entity.outboxpolling.mssql.OutboxPolling_Mssql;
import id.co.bfi.pollingpublisher.query.action.OutboxPollingAction;

@Service
public class OutboxPollingService_Mssql implements OutboxPollingService<OutboxPolling_Mssql> {

	@Autowired
	private OutboxPollingAction<OutboxPolling_Mssql> action;

	@Override
	public void convertAndPublish(OutboxPolling_Mssql outboxPolling) throws Exception {
		action.convertAndPublish(outboxPolling);
	}

	@Override
	public void deletePublishedBefore(LocalDateTime deleteDate) {
		action.deletePublishedBefore(deleteDate);
	}

	@Override
	public List<OutboxPolling_Mssql> findUnpublishedOutboxPolling() {
		return action.findUnpublishedOutboxPolling();
	}
}
