package id.co.bfi.pollingpublisher.query.action;

import org.springframework.stereotype.Component;

import id.co.bfi.pollingpublisher.entity.outboxpolling.mssql.OutboxPolling_Mssql;

@Component
public class OutboxPollingAction_Mssql extends OutboxPollingAction<OutboxPolling_Mssql> {

	public OutboxPollingAction_Mssql() {
		super(OutboxPolling_Mssql.class);
	}

}
