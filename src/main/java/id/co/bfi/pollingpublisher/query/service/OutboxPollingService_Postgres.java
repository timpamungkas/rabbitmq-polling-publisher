package id.co.bfi.pollingpublisher.query.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.bfi.pollingpublisher.entity.outboxpolling.postgres.OutboxPolling_Postgres;
import id.co.bfi.pollingpublisher.query.action.OutboxPollingAction;

@Service
public class OutboxPollingService_Postgres implements OutboxPollingService<OutboxPolling_Postgres> {

	@Autowired
	private OutboxPollingAction<OutboxPolling_Postgres> action;

	@Override
	public void convertAndPublish(OutboxPolling_Postgres outboxPolling) throws Exception {
		action.convertAndPublish(outboxPolling);
	}

	@Override
	public void deletePublishedBefore(LocalDateTime deleteDate) {
		action.deletePublishedBefore(deleteDate);
	}

	@Override
	public List<OutboxPolling_Postgres> findUnpublishedOutboxPolling() {
		return action.findUnpublishedOutboxPolling();
	}
}
