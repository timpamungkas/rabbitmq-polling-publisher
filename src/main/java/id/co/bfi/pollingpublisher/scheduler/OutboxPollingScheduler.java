package id.co.bfi.pollingpublisher.scheduler;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import id.co.bfi.pollingpublisher.entity.outboxpolling.OutboxPolling;
import id.co.bfi.pollingpublisher.query.service.OutboxPollingService;
import id.co.bfi.pollingpublisher.query.service.OutboxPollingService_Local;
import id.co.bfi.pollingpublisher.query.service.OutboxPollingService_Mssql;
import id.co.bfi.pollingpublisher.query.service.OutboxPollingService_Postgres;

@Component
public class OutboxPollingScheduler {

	private static final Logger LOG = LoggerFactory.getLogger(OutboxPollingScheduler.class);

	private ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);

	@Autowired
	private OutboxPollingService_Local outboxPollingService_Local;

	@Autowired
	private OutboxPollingService_Mssql outboxPollingService_Mssql;

	@Autowired
	private OutboxPollingService_Postgres outboxPollingService_Postgres;

	private List<OutboxPollingService<? extends OutboxPolling>> services;

	private <T extends OutboxPolling> CompletableFuture<Void> deletePublished(
			final OutboxPollingService<T> outboxPollingService, final LocalDateTime deleteDate) {
		return CompletableFuture.runAsync(() -> {
			outboxPollingService.deletePublishedBefore(deleteDate);
		}, executor);
	}

	private <T extends OutboxPolling> CompletableFuture<Void> pollAndPublish(
			final OutboxPollingService<T> outboxPollingService) {
		return CompletableFuture.runAsync(() -> {
			try {
				outboxPollingService.findUnpublishedOutboxPolling().forEach(o -> {
					try {
						LOG.debug("Publishing for {}, data {}", outboxPollingService.getClass().getSimpleName(), o);
						outboxPollingService.convertAndPublish(o);
					} catch (Exception e) {
						LOG.error("Can't convertAndPublish {}, because {}", o, e.getMessage());
					}
				});
			} catch (Exception e) {
				LOG.error(e.getMessage());
			}
		}, executor);
	}

	@PostConstruct
	private void postConstruct() {
		this.services = new ArrayList<>();

		services.add(outboxPollingService_Local);
		services.add(outboxPollingService_Mssql);
		services.add(outboxPollingService_Postgres);
	}

	@Scheduled(cron = "0 0 0 * * *")
	public void scheduleDeletePublished() {
		LOG.debug("Start scheduler with {} thread pool", Runtime.getRuntime().availableProcessors() * 2);
		final var deleteDate = LocalDateTime.now().minusDays(7).truncatedTo(ChronoUnit.DAYS);
		final var futures = new ArrayList<Future<Void>>(services.size());

		for (var service : services) {
			futures.add(deletePublished(service, deleteDate));
		}

		futures.forEach(f -> {
			try {
				f.get(10, TimeUnit.MINUTES);
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				LOG.warn("Deleting throw exception {} {}", e.getClass(), e.getMessage());
			}
		});
		LOG.debug("Scheduler ends");
	}

	@Scheduled(fixedDelay = 10000)
	public void schedulePollAndPublish() throws InterruptedException, ExecutionException, TimeoutException {
		LOG.debug("Start scheduler with {} thread pool", Runtime.getRuntime().availableProcessors() * 2);
		final var futures = new ArrayList<Future<Void>>(services.size());

		for (var service : services) {
			futures.add(pollAndPublish(service));
		}

		futures.forEach(f -> {
			try {
				f.get(10, TimeUnit.MINUTES);
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				LOG.warn("Publishing throw exception {} {}", e.getClass(), e.getMessage());
			}
		});
		LOG.debug("Scheduler ends");
	}

}
