package id.co.bfi.pollingpublisher.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources({ @PropertySource(factory = YamlPropertySourceFactory.class, value = { "classpath:outboxpolling.yml",
		"file:/conf/outboxpolling.yml", "file:c:/conf/outboxpolling.yml" }, ignoreResourceNotFound = true) })
public class OutboxPollingExternalConfig {
}
