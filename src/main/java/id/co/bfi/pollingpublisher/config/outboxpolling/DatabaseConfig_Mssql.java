package id.co.bfi.pollingpublisher.config.outboxpolling;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {
		"id.co.bfi.pollingpublisher.repository.outboxpolling.mssql" }, entityManagerFactoryRef = "entityManagerFactory_Mssql", transactionManagerRef = "transactionManager_Mssql")
public class DatabaseConfig_Mssql {

	@Bean(name = "dataSource_Mssql")
	@ConfigurationProperties(prefix = "outboxpolling.datasource.mssql")
	public DataSource dataSource() {
		return dataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}

	@Bean(name = "dataSourceProperties_Mssql")
	@ConfigurationProperties(prefix = "outboxpolling.datasource.mssql")
	public DataSourceProperties dataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean(name = "entityManagerFactory_Mssql")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(dataSource()).packages("id.co.bfi.pollingpublisher.entity.outboxpolling.mssql")
				.build();
	}

	@Bean(name = "transactionManager_Mssql")
	public PlatformTransactionManager transactionManager(
			@Qualifier("entityManagerFactory_Mssql") final EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}

}
