package id.co.bfi.pollingpublisher.config.outboxpolling;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {
		"id.co.bfi.pollingpublisher.repository.outboxpolling.postgres" }, entityManagerFactoryRef = "entityManagerFactory_Postgres", transactionManagerRef = "transactionManager_Postgres")
public class DatabaseConfig_Postgres {

	@Bean(name = "dataSource_Postgres")
	@ConfigurationProperties(prefix = "outboxpolling.datasource.postgres")
	public DataSource dataSource() {
		return dataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}

	@Bean(name = "dataSourceProperties_Postgres")
	@ConfigurationProperties(prefix = "outboxpolling.datasource.postgres")
	public DataSourceProperties dataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean(name = "entityManagerFactory_Postgres")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(dataSource()).packages("id.co.bfi.pollingpublisher.entity.outboxpolling.postgres")
				.build();
	}

	@Bean(name = "transactionManager_Postgres")
	public PlatformTransactionManager transactionManager(
			@Qualifier("entityManagerFactory_Postgres") final EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}

}
