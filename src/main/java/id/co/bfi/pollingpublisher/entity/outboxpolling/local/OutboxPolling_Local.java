package id.co.bfi.pollingpublisher.entity.outboxpolling.local;

import javax.persistence.Entity;
import javax.persistence.Table;

import id.co.bfi.pollingpublisher.entity.outboxpolling.OutboxPolling;

@Entity
@Table(name = "outbox_polling")
public class OutboxPolling_Local extends OutboxPolling {

}
