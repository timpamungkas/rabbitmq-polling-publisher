package id.co.bfi.pollingpublisher.entity.outboxpolling.postgres;

import javax.persistence.Entity;
import javax.persistence.Table;

import id.co.bfi.pollingpublisher.entity.outboxpolling.OutboxPolling;

@Entity
@Table(name = "outbox_polling")
public class OutboxPolling_Postgres extends OutboxPolling {

}
