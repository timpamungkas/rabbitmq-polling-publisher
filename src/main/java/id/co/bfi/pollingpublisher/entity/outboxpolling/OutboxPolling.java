package id.co.bfi.pollingpublisher.entity.outboxpolling;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class OutboxPolling {

	@Column(nullable = false, name = "changed_data")
	private String changedData;

	@CreatedDate
	@Column(name = "created_date")
	private LocalDateTime createdDate;

	@Column(name = "exchange", length = 200, nullable = false)
	private String exchange;

	@Id
	@JsonIgnore
	private long id;

	@Column(name = "note", length = 200)
	private String note;

	@Column(nullable = true, name = "published", length = 10)
	private String published;

	@Column(name = "routing_key", length = 200, nullable = false)
	private String routingKey;

	public OutboxPolling() {
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OutboxPolling other = (OutboxPolling) obj;
		if (changedData == null) {
			if (other.changedData != null)
				return false;
		} else if (!changedData.equals(other.changedData))
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (exchange == null) {
			if (other.exchange != null)
				return false;
		} else if (!exchange.equals(other.exchange))
			return false;
		if (id != other.id)
			return false;
		if (note == null) {
			if (other.note != null)
				return false;
		} else if (!note.equals(other.note))
			return false;
		if (routingKey == null) {
			if (other.routingKey != null)
				return false;
		} else if (!routingKey.equals(other.routingKey))
			return false;
		return true;
	}

	public String getChangedData() {
		return changedData;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public String getExchange() {
		return exchange;
	}

	public long getId() {
		return id;
	}

	public String getNote() {
		return note;
	}

	public String getPublished() {
		return published;
	}

	public String getRoutingKey() {
		return routingKey;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((changedData == null) ? 0 : changedData.hashCode());
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((exchange == null) ? 0 : exchange.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((note == null) ? 0 : note.hashCode());
		result = prime * result + ((routingKey == null) ? 0 : routingKey.hashCode());
		return result;
	}

	public void setChangedData(String changedData) {
		this.changedData = changedData;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setPublished(String published) {
		this.published = published;
	}

	public void setRoutingKey(String routingKey) {
		this.routingKey = routingKey;
	}

	@Override
	public String toString() {
		return "OutboxPolling [changedData=" + changedData + ", published=" + published + ", createdDate=" + createdDate
				+ ", exchange=" + exchange + ", id=" + id + ", note=" + note + ", routingKey=" + routingKey + "]";
	}

}
