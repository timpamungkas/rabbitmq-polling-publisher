package id.co.bfi.pollingpublisher.repository.outboxpolling;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface OutboxPollingRepository<T> extends PagingAndSortingRepository<T, Long> {

	void deletePublishedBefore(LocalDateTime deleteDate);

	List<T> findByPublishedIsNullOrderByIdAsc();

	void updatePublished(long id);

}
