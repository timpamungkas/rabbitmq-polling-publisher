package id.co.bfi.pollingpublisher.repository.outboxpolling.mssql;

import java.time.LocalDateTime;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.bfi.pollingpublisher.entity.outboxpolling.mssql.OutboxPolling_Mssql;
import id.co.bfi.pollingpublisher.repository.outboxpolling.OutboxPollingRepository;

@Repository
public interface OutboxPollingRepository_Mssql extends OutboxPollingRepository<OutboxPolling_Mssql> {

	@Override
	@Transactional
	@Modifying
	@Query("delete OutboxPolling_Mssql o where o.published = 'PUBLISHED' and o.createdDate < :deleteDate")
	void deletePublishedBefore(@Param("deleteDate") LocalDateTime deleteDate);

	@Override
	@Transactional
	@Modifying
	@Query("update OutboxPolling_Mssql o set o.published = 'PUBLISHED' where o.id = :id and o.published is null")
	void updatePublished(@Param("id") long id);

}
