package id.co.bfi.pollingpublisher.util;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.Clob;
import java.sql.SQLException;

import javax.sql.rowset.serial.SerialClob;
import javax.sql.rowset.serial.SerialException;
import javax.transaction.Transactional;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

@Component
public class ClobUtil {

	@Transactional
	public String clobToString(Clob clob) throws IOException, SQLException {
		var reader = clob.getCharacterStream();
		var writer = new StringWriter();
		IOUtils.copy(reader, writer);

		return writer.toString();
	}

	public Clob stringToClob(String str) throws SerialException, SQLException {
		return new SerialClob(str.toCharArray());
	}

}
