package id.co.bfi.pollingpublisher.broker.message;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class OutboxPollingMessage {

	private String changedData;
	@JsonIgnore
	private String exchange;
	private long id;

	@JsonIgnore
	private String routingKey;

	private String transactionType;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OutboxPollingMessage other = (OutboxPollingMessage) obj;
		if (changedData == null) {
			if (other.changedData != null)
				return false;
		} else if (!changedData.equals(other.changedData))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	public String getChangedData() {
		return changedData;
	}

	public String getExchange() {
		return exchange;
	}

	public long getId() {
		return id;
	}

	public String getRoutingKey() {
		return routingKey;
	}

	public String getTransactionType() {
		return transactionType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((changedData == null) ? 0 : changedData.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	public void setChangedData(String changedData) {
		this.changedData = changedData;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setRoutingKey(String routingKey) {
		this.routingKey = routingKey;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	@Override
	public String toString() {
		return "OutboxPollingMessage [changedData=" + changedData + ", id=" + id + ", transactionType="
				+ transactionType + ", exchange=" + exchange + ", routingKey=" + routingKey + "]";
	}

}
